interface Props {
    value: string;
}
export default function useFormElementEmit(
    event: Event,
    emit: any,
    props: Props
) {
    const updateValue = (event: Event) => {
        let value = (event.target as HTMLInputElement).value;

        if ((event.target as HTMLInputElement).type === 'radio')
            value = props.value;

        emit('update:modelValue', value);
    };

    return { updateValue };
}
