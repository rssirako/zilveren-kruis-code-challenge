export default function isValidBSN(bsn: string): boolean {
    if (bsn.length !== 9 || isNaN(+bsn[8])) return false;

    const sum = bsn
        .slice(0, 8)
        .split('')
        .reduce((acc, digit, i) => acc + +digit * (9 - i), 0);
    const checkDigit = (+bsn[8] + 10) % 11;

    return (sum + checkDigit) % 11 === 0;
}