import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

interface SelectedPackage {
    basicIns: string | null;
    deductable: number  | null;
    additIns: string  | null;
    dentalIns: string  | null;
}

interface TotalPremium {
    paymentTerm: string  | null;
    totalPremium: string  | null;

}
interface ContactDetails {
    name: string  | null;
    sex: string  | null;
    dob: string  | null;
    bsn: string  | null;
}
interface FormInfo {
    selectedPackage: SelectedPackage;
    totalPremium: TotalPremium;
    contactdetails: ContactDetails;
}
export const useFormInfo = defineStore('formInfo', {
    state: () => {
        return {
            formInfo: <FormInfo>{}
        };
    },
    getters: {
        getFormInfo: state => {
            return state.formInfo;
        }
    },
    actions: {
        setFormInfo(data: FormInfo) {
            this.formInfo = data;
        },
    }
})
