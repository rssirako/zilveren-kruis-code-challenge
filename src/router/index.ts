import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '../views/HomeView.vue'
import FormSubmittedView from '../views/FormSubmittedView.vue'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'home',
            component: HomeView
        },
        {
            path: '/form-submitted',
            name: 'form-sumbitted',
            component: FormSubmittedView,
        }]
});

export default router;
