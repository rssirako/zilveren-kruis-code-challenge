<script setup lang="ts">
import { ref } from 'vue';

import useFormElementEmit from '../composables/useFormElementEmit.ts';
import useIsValidBSN from '../composables/useIsValidBSN.ts';

const props = defineProps({
    label: {
        type: String,
        required: true
    },
    value: {
        type: String,
        default: ''
    },
    idValue: {
        type: String,
        default: ''
    },
    modelValue: {
        type: String
    }
});
const emit = defineEmits(['update:modelValue']);
const { updateValue } = useFormElementEmit(event, emit, props);

const isError = ref(false);
const validate = () => {
    if (props.label !== 'Burgerservicenummer') return;
     //TODO haven't tested but couple ms timeout might be needed for the value to propagate from parent 
    isError.value = useIsValidBSN(props.modelValue);
};
</script>

<template>
    <div class="form-input my-4">
        <div class="input__group">
            <label class="input__title" v-if="label" :for="idValue">
                {{ label }}
            </label>
            <input
                class="input__field form-control"
                type="text"
                @input="updateValue($event)"
                :id="idValue"
                :value="modelValue"
                @blur="validate"
            />
            <ErrorMessage v-if="isError" id="bsn-error">
                Helaas is het ingevoerde burgerservicenummer niet geldig.
                Probeer het opnieuw.
            </ErrorMessage>
        </div>
    </div>
</template>
