/* eslint-disable */
/* prettier-ignore */
// @ts-nocheck
// Generated by unplugin-vue-components
// Read more: https://github.com/vuejs/core/pull/3399
export {}

declare module 'vue' {
  export interface GlobalComponents {
    AppHeader: typeof import('./src/components/AppHeader.vue')['default']
    DatePicker: typeof import('./src/components/DatePicker.vue')['default']
    DropDown: typeof import('./src/components/DropDown.vue')['default']
    ErrorMessage: typeof import('./src/components/ErrorMessage.vue')['default']
    Footer: typeof import('./src/components/Footer.vue')['default']
    InputField: typeof import('./src/components/InputField.vue')['default']
    InsuranceFormWrapper: typeof import('./src/components/InsuranceFormWrapper.vue')['default']
    RadioButton: typeof import('./src/components/RadioButton.vue')['default']
    RadioGroup: typeof import('./src/components/RadioGroup.vue')['default']
    RouterLink: typeof import('vue-router')['RouterLink']
    RouterView: typeof import('vue-router')['RouterView']
    Test: typeof import('./src/components/test.vue')['default']
    TopNavBar: typeof import('./src/components/TopNavBar.vue')['default']
  }
}
